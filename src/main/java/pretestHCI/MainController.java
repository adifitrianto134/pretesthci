package pretestHCI;

import java.util.Scanner;
import java.util.regex.Pattern;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MainController {
	
	@RequestMapping(value="preTest")
	public @ResponseBody String preTest() {
		//initiation
		System.out.println("input array length : ");
		Scanner myObj = new Scanner(System.in);
		Integer limit = myObj.nextInt();
		Object[] arr = new Object[limit];
		
		for(int a=0;a<limit;a++) {
			System.out.println("enter the value of index - "+ a + " :");
			Scanner each = new Scanner(System.in);
			String input = each.nextLine();
			Pattern pattern = Pattern.compile(".*[^0-9].*");
			if(!pattern.matcher(input).matches()) {
				arr[a] = Integer.parseInt(input);
			}
			else {
				arr[a] = input.charAt(0);
			}
		}
		
//		Object[] arr = {'a',1,'d',3,'c',5,'b',2,'e',4};
		Integer charCounter=0, intCounter = 0, generalCounter=0;
		Object temporaryObject = new Object();
		String result = "";
		Integer Indexcounter = 0;
		Integer valueCounter = 0;
		
	
		//count each type of array
		for(int a=0;a<arr.length;a++) {
			if(arr[a] instanceof Integer) {
				intCounter++;
			}
			else {
				charCounter++;
			}
		}
		
		
		//create and fill 2 type of array
		char[] arrChar = new char[charCounter];
		int[] arrInt = new int[intCounter];
		for(int a=0;a<arr.length;a++) {
			if(arr[a] instanceof Integer) {
				arrInt[generalCounter] =  (Integer) arr[a];
				generalCounter++;
			}
		}
		generalCounter = 0;
		for(int a=0;a<arr.length;a++) {
			if(! (arr[a] instanceof Integer) ) {
				arrChar[generalCounter] = (char) arr[a];
				generalCounter++;
			}
		}
		
		
		//start asc sorting for both arrays
		for(int a=0;a<arrChar.length;a++) {
			for(int b=a+1;b<arrChar.length;b++) {
				if(arrChar[b] < arrChar[a]) {
					char temp1 = arrChar[a];
					char temp2 = arrChar[b];
					arrChar[a] = temp2;
					arrChar[b] = temp1;
				}
			}
		}
		for(int a=0;a<arrInt.length;a++) {
			for(int b=a+1;b<arrInt.length;b++) {
				if(arrInt[b] < arrInt[a]) {
					Integer temp1 = arrInt[a];
					Integer temp2 = arrInt[b];
					arrInt[a] = temp2;
					arrInt[b] = temp1;
				}
			}
		}
		
		
		//merge both arrays
		for(int a=0;a<arr.length;a++) {
			if(a<arrChar.length) {
				arr[a] = arrChar[a];
			}
			else {
				arr[a] = 0;
			}
		}
		Indexcounter = arrChar.length;
		valueCounter = 0;
		for(int a=arrChar.length;a<arr.length;a++) {
			arr[Indexcounter] = arrInt[valueCounter];
			Indexcounter++;
			valueCounter++;
		}
		
		
		// print output
		for(int a=0;a<arr.length;a++) {
			result = result + arr[a] + " ";
		}
		System.out.println(result);
		result = "";
		for(int a=0;a<arrChar.length;a++) {
			generalCounter=1;
			temporaryObject = arr[0];
			for(int b=0;b<arr.length;b++) {
				if(b != arr.length-1) {
					arr[b] = arr[generalCounter];
					generalCounter++;
				}
				else if(b == arr.length-1) {
					arr[b] = temporaryObject;
				}
				result = result + arr[b] + " ";
			}
			System.out.println(result);
			result = "";
		}
		
		return "";
	}

}
